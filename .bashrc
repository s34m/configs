#
# ~/.bashrc
#


#PATHS
PATH="`ruby -e 'puts Gem.user_dir'`/bin:$PATH"

# If not running interactively, don't do anything
export EDITOR="nvim"
export Visual="emacs"

PS1='[\u@\h \W]\$ '

#configcommands
alias configvim='vim ~/.config/nvim/init.vim'
alias configqtile='vim ~/.config/qtile/config.py'
alias configbashrc='vim ~/.bashrc'
alias configi3='vim ~/.config/i3/config'
alias configawesome='vim ~/.config/awesome/rc.lua'
alias configspectrwm='vim ~/.config/spectrwm/spectrwm.conf'
alias configxmonad='vim ~/.xmonad/xmonad.hs'
alias configvariety='vim ~/.config/variety/variety.conf'
alias configopenbox='vim ~/.config/openbox/rc.xml'
alias configbspwm='vim ~/.config/bspwm/bspwmrc'
alias configautostart='vim ~/.scripts/system/autostart.sh'
alias configsxhkd='vim ~/.config/sxhkd/sxhkdrc'
alias configherbstluftwm='vim ~/.config/herbstluftwm/autostart'



alias updatepackagelist='pacman -Qqe > pkglist.txt'

#Programming

alias tourofgo='/home/thomas/go/bin/tour -openbrowser false'
#TeX
alias texbp='cp ~/.scripts/texbp.tex'
#Docker
alias docker='sudo docker'

alias home='/home/thomas/.screenlayout/homeA.sh'

alias gc='git clone '
#alias ls='lsd'
#alias la='lsd -A'
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'
alias quote='fortune | cowsay | lolcat'
alias readit='echo "Read the fuckiong manual" | cowsay | lolcat '
alias aur='cd ~/AUR/ && git clone'
alias starwars='telnet towel.blinkenlights.nl'
alias lsds='lsd | lolcat'
alias aquarium='asciiquarium'
alias vim='nvim'
alias vimplugin='cd ~/.local/share/nvim/site/pack/default/start '
alias ..='cd ..'
alias nvimconfig='vim ~/.config/nvim/init.vim'
alias macho='~/.scripts/macho.sh'
alias doom='~/.emacs.d/bin/doom'
alias update='yay -Syyu"'
alias unlock='sudo rm /var/lib/pacman/db.lck'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias df='df -h'
alias logout='arcolinux-logout'
alias free='free -mt'
alias userlist='cut -d: -f1 /etc/passwd'
alias upall='yay -Syu --noconfirm'
alias skel='cp -Rf ~/.config ~/.config-backup-$(date +%Y.%m.%d-%H.%M.%S) && cp -rf /etc/skel/* ~'
alias bupskel='cp -Rf /etc/skel ~/.skel-backup-$(date +%Y.%m.%d-%H.%M.%S)'
alias yayskip='yay -S --mflags --skipinteg'
alias trizenskip='trizen -S --skipinteg'
alias rip="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -200 | nl"
alias riplong="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -3000 | nl"
alias iso="cat /etc/dev-rel | awk -F '=' '/ISO/ {print $2}'"
alias neofetch='neofetch | lolcat'
alias emacs='emacsclient -ncu'
alias em='/usr/bin/emacs -nw'
alias :q='exit'

#git bares
alias gitschule='/usr/bin/git --git-dir=$HOME/.gitbare/schule/ --work-tree=$HOME'
alias gitconfig='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
alias gitprivat='/usr/bin/git --git-dir=$HOME/.gitbare/privat --work-tree=$HOME'
alias gitmc='/usr/bin/git --git-dir=$HOME/.gitbare/multimc --work-tree=$HOME'

#youtube-dl
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "
alias ytv-best="youtube-dl -f bestvideo+bestaudio "
alias yt="youtube-dl -o '/home/thomas/Videos/Projekte/Hausübung/%(title)s.%(ext)s'"
alias media="youtube-dl -o '/home/thomas/Videos/%(title)s.%(ext)s'"

#flags
alias cp="cp -i"
alias df='df -h'

# # ex = EXtractor for all kinds of archives
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;      
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}



#powerline-shell start
#
#

function _update_ps1() {
    PS1=$(powerline-shell $?)
}

if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
fi

#
#
#powerline-shell end


echo "      You are running GNU/LINUX not WINDOWS" | lolcat
ufetch-arco | lolcat
echo "            You should never forget" | lolcat
echo "        Linux loves you Windows doesn't" | lolcat
echo "                    RTFM" | lolcat


source /home/thomas/.config/broot/launcher/bash/br
