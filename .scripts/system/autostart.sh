#!/bin/bash
variety &
nm-applet
xfce4-power-manager
numlockx off
blueberry-tray
pamac-tray
picom
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
/usr/lib/xfce4/notifyd/xfce4-notifyd
flameshiot
feh --bg-fill /usr/share/backgrounds/arcolinux/arco-wallpaper.jpg
volumeicon
spotify
copyq
~/.scripts/system/start-sxhkd.sh
xsetroot -cursor_name left_ptr &
picom

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

feh --bg-fill /usr/share/backgrounds/arcolinux/arco-wallpaper.jpg &

run variety &
run nm-applet &
run pamac-tray &
run xfce4-power-manager &
numlockx off &
blueberry-tray &
picom --config $HOME/.config/qtile/scripts/picom.conf &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
run volumeicon &
run spotify &
run emacsclient -ncu

