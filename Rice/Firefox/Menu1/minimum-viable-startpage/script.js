const NAME = "Thomas";
const WELCOME_MESSAGE_TEMPLATE = ["night", "morning", "afternoon", "evening"];

const SHORTCUT_STARTER = 'tab' 

const SHORTCUT_TIMEOUT = 1500;

const MASTER_MAP = [
    {
        "groupName": "IT",
        "items":[
            {"name": "DEVDOCS.IO", "url": "https://devdocs.io/"},
            {"name": "STACKOVERFLOW", "url": "https://stackoverflow.com/"},
            {"name": "UDEMY", "url": "https://www.udemy.com"},
            {"name": "ATTLASIAN", "url": "https://start.atlassian.com/"},
            {"name": "CODEWARS", "url": "https://www.codewars.com"},
            {"name": "Figma", "url": "https://www.figma.com/"},
            {"name": "Flare", "url": "https://rive.app/"},
            {"name": "Font Flipper", "url": "https://fontflipper.com/"}
        ]
    },
    {
        "groupName": "GIT",
        "items":[
            {"name": "GITHUB", "url": "https://github.com/"},
            {"name": "BITBUCKET", "url": "https://bitbucket.org/dashboard/overview"},
        ]
    },
    {
        "groupName" : "EMACS",
        "items":[
            {"name": "EMACS", "url": "https://www.gnu.org/software/emacs/"},
            {"name": "EMACS-WIKI", "url": "https://www.emacswiki.org/"},
            {"name": "ORG-MODE", "url": "https://orgmode.org/"},
            {"name": "ORG-MODE-WIKI", "url": "https://orgmode.org/org.html#Conventions"},
            {"name": "MAGIT WIKI", "url": "https://magit.vc/manual/magit/"}
            
        ]
    },
    {
        "groupName": "SCHULE",
        "items":[
            {"name": "BRG WELS", "url": "https://www.brgwels.at/"},
            {"name": "WEBUNTIS", "url": "https://klio.webuntis.com/WebUntis/?school=brgwels#/basic/main"},
            {"name": "MOODLE", "url": "https://www3.lernplattform.schule.at/brgwels/login/index.php"},
	    {"name": "DIGI4SCHOOL", "url": "https://digi4school.at/ebooks"}
        ]
    },

    {
        "groupName": "GAMES",
        "items":[
            {"name": "STEAM", "url": "https://store.steampowered.com/"},
            {"name": "GAMESTAR", "url": "https://www.gamestar.de"},
            {"name": "METACRITIC GAMES", "url": "https://www.metacritic.com/game"}
        ]
    },
    {
        "groupName": "WIKI",
        "items":[
            {"name": "ARCH WIKI", "url": "https://wiki.archlinux.org/"},
            {"name": "ARCH APPS", "url": "https://wiki.archlinux.org/index.php/List_of_applications"},
            {"name": "WIKIPEDIA", "url": "https://www.wikipedia.org/"},
            {"name": "UNREAL ENGINE DOCS", "url": "https://docs.unrealengine.com/en-US/index.html"}
        ]
    },
    {
        "groupName": "SOCIAL MEDIA",
        "items":[
            {"name": "WHATSAPP", "url": "https://web.whatsapp.com/"},
            {"name": "INSTAGRAM", "url": "https://www.instagram.com/"},
            {"name": "YOUTUBE", "url": "https://www.youtube.com/"},
            {"name": "REDDIT", "url": "https://www.reddit.com/"}
        ]
    },
    {
        "groupName": "NACHRICHTEN",
        "items":[
            {"name": "ORF", "url": "https://www.orf.at/"},
            {"name": "REUTERS", "url": "https://www.reuters.com/"},
            {"name": "CNN", "url": "https://edition.cnn.com/"},
	    {"name": "Tagespresse", "url": "https://dietagespresse.com/"} 
       ]
    },
    {
        "groupName": "Hacking",
        "items":[
        	{"name": "PasswordStats", "url": "https://wpengine.com/unmasked/"},
		{"name": "GeekTyper", "url": "https://geektyper.com/"},
		{"name": "PasswordStats", "url": "https://wpengine.com/unmasked/"},
		{"name": "PasswordStats", "url": "https://wpengine.com/unmasked/"}
       ]
    }
]

let $container = document.getElementById("content");
let getUrl = {};

let $shortcutDisplayList = document.getElementsByClassName("shortcut");
let listeningForShortcut = false;
let listenerTimeout;

function setupWelcomeMessage(){
    let curHours = new Date().getHours();
    curHours = Math.floor(curHours/6);
    if (curHours == 4) curHours = 3;
    let welcome = "Good " + WELCOME_MESSAGE_TEMPLATE[curHours] + ", " + NAME;
    document.getElementById("welcome-string").innerHTML = welcome;
}

function setupGroups(){
    for (let i = 0; i < MASTER_MAP.length; i++){
        let curGroupData = MASTER_MAP[i];

        let group = document.createElement("div");
        group.className = "group";
        $container.appendChild(group);

        let header = document.createElement("h1");
        header.innerHTML = curGroupData.groupName;
        group.appendChild(header);

        for (let j = 0; j < curGroupData.items.length; j++){
            let curItemData = curGroupData.items[j];

            let pContainer = document.createElement("p");
            group.appendChild(pContainer);

            let link = document.createElement("a");
            link.innerHTML = curItemData.name;
            link.setAttribute("href", curItemData.url);
            pContainer.appendChild(link);

            let shortcutDisplay = document.createElement("span");
            shortcutDisplay.innerHTML = curItemData.shortcutKey;
            shortcutDisplay.className = "shortcut";
            shortcutDisplay.style.animation = "none";
            pContainer.appendChild(shortcutDisplay);

            getUrl[curItemData.shortcutKey] = curItemData.url
        }
    }
}

function shortcutListener(e) {
    let key = e.key.toLowerCase();

    if (listeningForShortcut && getUrl.hasOwnProperty(key)){
        window.location = getUrl[key];
    }

    if (key === SHORTCUT_STARTER) {
        clearTimeout(listenerTimeout);
        listeningForShortcut = true;

        // Animation reset
        for (let i = 0; i < $shortcutDisplayList.length; i++){
            $shortcutDisplayList[i].style.animation = "none";
            setTimeout(function() { $shortcutDisplayList[i].style.animation = ''; }, 10);
        }

        listenerTimeout = setTimeout(function(){ listeningForShortcut = false; }, SHORTCUT_TIMEOUT);
    }
}

function main(){
    setupWelcomeMessage();
    setupGroups();
    document.addEventListener('keyup', shortcutListener, false);
}

main();
