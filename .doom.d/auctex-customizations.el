;;; ~/.doom.d/auctex-customizations.el -*- lexical-binding: t; -*-

;; The main thing to modify is LaTeX-set-unicode-p below, which
;; controls which abbreviations are converted to Unicode, and
;; LaTeX-math-list, to add additional symbols that can be turned
;; into Unicode.

;; LaTeX-math-list is a variable that you can set to add more
;; symbols to AUCTeX's abbreviations. The default list is at
;; LaTeX-math-default. If you add to this, you probably also
;; want to change LaTeX-set-unicode-p.
(custom-set-variables
 '(LaTeX-math-list
   '(
    ("v d" "partial" "Misc Symbol" 8706) ;; #X2202
    ("o p" "oplus" "Binary Op" 8853) ;; #X2295
    ("o m" "ominus" "Binary Op" 8854) ;; #X2296
    ("o t" "otimes" "Binary Op" 8855) ;; #X2297
    ("o s" "oslash" "Binary Op" 8709) ;; #X2205
    ("o d" "odot" "Binary Op" 8857) ;; #X2299
    ("o S" "sum" "Var Symbol" 8721) ;; #X2211
    ("o P" "prod" "Var Symbol" 8719) ;; #X220F
    ("o C" "coprod" "Var Symbol" 8720) ;; #X2210
    ("o I" "int" "Var Symbol" 8747) ;; #X222B
    ("o o I" "oint" "Var Symbol" 8750) ;; #X222E
    ("o g" "digamma" ("AMS" "Greek Lowercase") 989) ;; #X03DD
    )))

;; The functions below were adapted from LaTeX-math-initialize.

(defun LaTeX-set-unicode-math-list-entry (entry)
  "Given an entry, which should be a list with up to four elements,
KEY, VALUE, MENU, and CHARACTER (see `LaTeX-math-list'), if
CHARACTER decodes to a valid Unicode character, redefine
LaTeX-math-VALUE to simply insert CHARACTER."
  (when (listp (cdr entry))
    (let* ((value (nth 1 entry))
           (char (nth 3 entry))
           (uchar (and char (decode-char 'ucs char))))
      (when (and (stringp value) uchar)
        (let ((name (intern (concat "LaTeX-math-" value))))
          ;; Is there a more elegant way to do this than to just
          ;; manually build the list?
          (fset name (list 'lambda (list) (list 'interactive "*")
                           (list 'insert uchar))))))))

(defun LaTeX-set-unicode-math-list (list set-unicode-p)
  "Given a list of entries, each of which should be a list with
up to four elements, KEY, VALUE, MENU and CHARACTER (see
`LaTeX-math-list'), call LaTeX-set-unicode-math-list-entry for
every entry for which \(set-unicode-p VALUE MENU\) returns true."
  (dolist (entry list)
    (when (listp (cdr entry))
      (let* ((value (nth 1 entry))
             (menu (nth 2 entry)))
        (when (funcall set-unicode-p value menu)
          (LaTeX-set-unicode-math-list-entry entry))))))

;; For now, return true for Greek letters, symbols from
;; LaTeX-math-list with key bindings, and some other math symbols.
(defun LaTeX-set-unicode-p (value menu)
  (or
   ;; Symbols under Math > Greek Uppercase and Math > Greek Lowercase.
   (and (stringp menu)
        (string-prefix-p "Greek" menu))
   ;; More Greek letters under Math > AMS > Greek Uppercase and Math >
   ;; AMS > Greek Lowercase.
   (and (listp menu)
        (string-prefix-p "Greek" (nth 1 menu)))
   ;; Symbols defined in LaTeX-math-list with a key binding.
   (member value (mapcar (lambda (x) (nth 1 x))
                         (seq-filter (lambda (x) (nth 0 x)) LaTeX-math-list)))
   ;; Misc. symbols from LaTeX-math-default.
   (member value '("wedge" "nabla"))))

(defun LaTeX-unicode-math-initialize ()
  (LaTeX-set-unicode-math-list
   (reverse (append LaTeX-math-list LaTeX-math-default))
   'LaTeX-set-unicode-p))

;; Add a hook to do the Unicode conversion.
(add-hook 'LaTeX-mode-hook 'LaTeX-unicode-math-initialize)
