(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-ispell-fuzzy-limit 4)
 '(ac-ispell-requires 4)
 '(ansi-color-names-vector
   ["#1e1e1e" "#fb4934" "#b8bb26" "#fabd2f" "#83a598" "#cc241d" "#8ec07c" "#ebdbb2"])
 '(custom-safe-themes
   '("4bca89c1004e24981c840d3a32755bf859a6910c65b829d9441814000cf6c3d0" "76bfa9318742342233d8b0b42e824130b3a50dcc732866ff8e47366aed69de11" "c4bdbbd52c8e07112d1bfd00fee22bf0f25e727e95623ecb20c4fa098b74c1bd" "3df5335c36b40e417fec0392532c1b82b79114a05d5ade62cfe3de63a59bc5c6" "a3b6a3708c6692674196266aad1cb19188a6da7b4f961e1369a68f06577afa16" "79278310dd6cacf2d2f491063c4ab8b129fee2a498e4c25912ddaa6c3c5b621e" "2f1518e906a8b60fac943d02ad415f1d8b3933a5a7f75e307e6e9a26ef5bf570" "c83c095dd01cde64b631fb0fe5980587deec3834dc55144a6e78ff91ebc80b19" "2cd603dfa05f7397a1f5e9d66e87d37c9ab94437a387f6df8b93cb6c62dc2e48" "912cac216b96560654f4f15a3a4d8ba47d9c604cbc3b04801e465fb67a0234f0" "be4091bea5eadc90c64713b41b877c30b5481622c232abc3e6da12a218eb146a" "27b9ae6753e97ea1edfb9e6df748823fe30d66069020e91effd01538775ad352" "996768b0d25f88b3a403f3d5e54cbc8f0480284436dba311a39ecb08866803e6" "7191bfdcacfe59a4786439642a331c80fbd1b5500d1435235103e84129b8d244" "369fa261985327467e286a582fe324ce21a05f55fc0ef234745bf209e309a5c0" "935a7ca6e72e2bcebeb434b27b1a61be85bc0b42d6aea795a5f4ad6fe184c7c2" "3577ee091e1d318c49889574a31175970472f6f182a9789f1a3e9e4513641d86" "632694fd8a835e85bcc8b7bb5c1df1a0164689bc6009864faed38a9142b97057" "e2acbf379aa541e07373395b977a99c878c30f20c3761aac23e9223345526bcc" default))
 '(dimmer-mode t nil (dimmer))
 '(dtrt-indent-max-lines 1500)
 '(elfeed-feeds
   '("http://feeds.bbci.co.uk/news/technology/rss.xml" "http://feeds.bbci.co.uk/news/science_and_environment/rss.xml" "http://feeds.bbci.co.uk/news/world/rss.xml#" "https://rss.nytimes.com/services/xml/rss/nyt/World.xml" "https://feed.infoq.com/"))
 '(fci-rule-color "#7c6f64")
 '(jdee-db-active-breakpoint-face-colors (cons "#0d1011" "#fabd2f"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#0d1011" "#b8bb26"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#0d1011" "#928374"))
 '(objed-cursor-color "#fb4934")
 '(org-agenda-files '("/home/thomas/org/index.org"))
 '(pdf-view-midnight-colors (cons "#ebdbb2" "#282828"))
 '(rainbow-delimiters-max-face-count 3)
 '(rainbow-delimiters-outermost-only-face-count 0)
 '(rainbow-delimiters-pick-face-function 'rainbow-delimiters-default-pick-face)
 '(rustic-ansi-faces
   ["#282828" "#fb4934" "#b8bb26" "#fabd2f" "#83a598" "#cc241d" "#8ec07c" "#ebdbb2"])
 '(vc-annotate-background "#282828")
 '(vc-annotate-color-map
   (list
    (cons 20 "#b8bb26")
    (cons 40 "#cebb29")
    (cons 60 "#e3bc2c")
    (cons 80 "#fabd2f")
    (cons 100 "#fba827")
    (cons 120 "#fc9420")
    (cons 140 "#fe8019")
    (cons 160 "#ed611a")
    (cons 180 "#dc421b")
    (cons 200 "#cc241d")
    (cons 220 "#db3024")
    (cons 240 "#eb3c2c")
    (cons 260 "#fb4934")
    (cons 280 "#e05744")
    (cons 300 "#c66554")
    (cons 320 "#ac7464")
    (cons 340 "#7c6f64")
    (cons 360 "#7c6f64")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-scrollbar-bg ((t (:background "#3df1410a539f"))))
 '(company-scrollbar-fg ((t (:background "#330c359a44ea"))))
 '(company-tooltip ((t (:inherit default :background "#2c832ebd3c17"))))
 '(company-tooltip-common ((t (:inherit font-lock-constant-face))))
 '(company-tooltip-selection ((t (:inherit font-lock-function-name-face)))))
