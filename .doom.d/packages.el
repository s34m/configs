;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; To install a package with Doom you must declare them here and run 'doom sync'
;; on the command line, then restart Emacs for the changes to take effect -- or
;; use 'M-x doom/reload'.


;; To install SOME-PACKAGE from MELPA, ELPA or emacsmirror:
;(package! some-package)

;; To install a package directly from a remote git repo, you must specify a
;; `:recipe'. You'll find documentation on what `:recipe' accepts here:
;; https://github.com/raxod502/straight.el#the-recipe-format
;(package! another-package
;  :recipe (:host github :repo "username/repo"))

;; If the package you are trying to install does not contain a PACKAGENAME.el
;; file, or is located in a subdirectory of the repo, you'll need to specify
;; `:files' in the `:recipe':
;(package! this-package
;  :recipe (:host github :repo "username/repo"
;           :files ("some-file.el" "src/lisp/*.el")))

;; If you'd like to disable a package included with Doom, you can do so here
;; with the `:disable' property:
;(package! builtin-package :disable t)

;; You can override the recipe of a built in package without having to specify
;; all the properties for `:recipe'. These will inherit the rest of its recipe
;; from Doom or MELPA/ELPA/Emacsmirror:
;(package! builtin-package :recipe (:nonrecursive t))
;(package! builtin-package-2 :recipe (:repo "myfork/package"))

;; Specify a `:branch' to install a package from a particular branch or tag.
;; This is required for some packages whose default branch isn't 'master' (which
;; our package manager can't deal with; see raxod502/straight.el#279)
;(package! builtin-package :recipe (:branch "develop"))

;; Use `:pin' to specify a particular commit to install.
;(package! builtin-package :pin "1a2b3c4d5e")


;; Doom's packages are pinned to a specific commit and updated from release to
;; release. The `unpin!' macro allows you to unpin single packages...
;(unpin! pinned-package)
;; ...or multiple packages
;(unpin! pinned-package another-pinned-package)
;; ...Or *all* packages (NOT RECOMMENDED; will likely break things)
;(unpin! t)

;;Fuzzy
(package! fuzzy)

;;AUCTEX Tex Addon
(package! auctex)
(package! reftex)

;;Slime IDE
(package! slime)
(package! elisp-slime-nav)

;;Auto Complete
;;(package! auto-complete)
;;(package! auto-complete-auctex)
;;(package! ac-math)
;;(package! ac-ispell)
;;(package! auto-complete-c-headers)
;;(package! org-ac)
;;(package! ac-octave)
;;(package! ac-php)
;;(package! ac-haskell-process)
;;(package! auto-complete-nxml)
;;(package! ac-clang)
;;
;;Company auto completion
(package! company)
(package! company-anaconda)
(package! company-arduino)
(package! company-auctex)
(package! company-bibtex)
(package! company-box)
(package! company-c-headers)
(package! company-flow)
(package! company-fuzzy)
(package! company-go)
(package! company-lua)
(package! company-math)
(package! company-php)
(package! company-plsense)

;;LISP
(package! evil-paredit)
(package! paredit-menu)
(package! parinfer)
(package! geiser)

;;Rainbow
(package! rainbow-delimiters)
(package! rainbow-mode)

;;HELM
(package! ess)
(package! helm)
(package! helm-R)
(package! helm-bibtex)
(package! helm-bibtexkey)
(package! helm-charinfo)
(package! helm-codesearch)
(package! helm-company)
(package! helm-css-scss)
(package! helm-dictionary)
(package! helm-directory)
(package! helm-evil-markers)
(package! helm-file-preview)
(package! helm-filesets)
(package! helm-flyspell)
(package! helm-fuzzy)
(package! helm-jira)
(package! helm-lib-babel)
(package! helm-lines)
(package! helm-mode-manager)
(package! helm-navi)
(package! helm-org)
(package! helm-org-multi-wiki)
(package! helm-pages)
(package! helm-phpunit)
(package! helm-projectile)
(package! helm-rails)
(package! helm-slime)
(package! helm-spotify)
(package! helm-spotify-plus)
(package! helm-sql-connect)
(package! helm-system-packages)
(package! helm-systemd)
(package! helm-unicode)
(package! helm-xref)
(package! helm-youtube)
(package! math-symbols)
(package! org-books)
(package! org-projectile-helm)
(package! org-ref)
(package! typo-suggest)
(package! helm-taskswitch)

;;Enable Dimmer
(package! dimmer)

;;Helpful
(package! helpful)

;;Wordcount
(package! wc-mode)

(package! writeroom-mode)

;;Zotelo Mode for TeX
(package! zotelo)

;;ERC
(package! erc-hl-nicks)
(package! erc-status-sidebar)

;;YoutubeDL
(package! ytdl)

;;Enable Spotify
(package! spotify)


;;Fun
(package! elcord)
(package! selectric-mode)
(package! keycast)
(package! info-colors)
(package! org-pretty-table-mode :recipe (:host github :repo "Fuco1/org-pretty-table"))
(package! systemd)

(package! spell-fu :disable t)

(package! all-the-icons-dired)

(package! line-reminder)

(package! memento-mori)
