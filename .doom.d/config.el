;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!
(require 'color)

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Thomas Schoenauer"
      user-mail-address "t.schoenauer@hgs-wt.at")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;;
 (setq doom-font (font-spec :family "Fira Code" :size 15 :weight 'medium)
       doom-variable-pitch-font (font-spec :family "Fira Code" :size 18)
       doom-big-font (font-spec :family"Fira Code" :size 20  :weight 'semi-bold))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-palenight)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
;;
;;
;;
;;Icons scaling to 1
(setq all-the-icons-scale-factor 1.0)

;;make flyspell faster
(setq flyspell-issue-message-flag 'nil)

;;ORG-Mode disable invisible edits
(setq org-catch-invisible-edits 'error)

;;Enable Company AC
(add-to-list 'company-backends 'company-math-symbols-unicode)
(setq company-tooltip-align-annotations t)
(add-hook 'after-init-hook 'global-company-mode)

  (let ((bg (face-attribute 'default :background)))
    (custom-set-faces
     `(company-tooltip ((t (:inherit default :background ,(color-lighten-name bg 2)))))
     `(company-scrollbar-bg ((t (:background ,(color-lighten-name bg 10)))))
     `(company-scrollbar-fg ((t (:background ,(color-lighten-name bg 5)))))
     `(company-tooltip-selection ((t (:inherit font-lock-function-name-face))))
     `(company-tooltip-common ((t (:inherit font-lock-constant-face))))))

(setq company-selection-wrap-around t)
(setq company-minimum-prefix-length 1)
(setq company-idle-delay 0)


;;Latex Menubar
;;(add-hook! 'reftex-load-hook 'imenu-add-menubar-index)
;;(add-hook! 'reftex-mode-hook 'imenu-add-menubar-index)


;;enable Ref Te X
(setq reftex-plug-into-AUCTeX t)

;;AUCTeX
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)
(setq TeX-PDF-mode t)

(add-hook 'TeX-mode-hook 'zotelo-minor-mode)

(require 'reftex)


;;Enable SLIME IDE
(setq inferior-lisp-program "sbcl")

(setq auto-complete-mode t)

;;Enable Orgstruct
(add-hook 'message-mode-hook 'turn-on-orgstruct)
(add-hook 'message-mode-hook 'turn-on-orgstruct++)

;;Enable ORG Agenda Stuff
(setq org-agenda-include-diary t)
(setq org-agenda-include-deadlines t)
(setq org-agenda-include-inactive-timestamps t)

;;ORG CALENDAR COORDINATES AND TIME
(setq calendar-latitude 48.1)
(setq calendar-longitude 14.0)
(setq calendar-location-name "Wels, AT")
(setq calendar-standard-time-zone-name "CEST")
(setq calendar-daylight-time-zone-name "CET")
(setq calendar-time-zone +60)
;;Enable Dired-X
     (add-hook 'dired-load-hook
               (lambda ()
                 (load "dired-x")
                 ;; Set dired-x global variables here.  For example:
                 ;; (setq dired-guess-shell-gnutar "gtar")
                 ;; (setq dired-x-hands-off-my-keys nil)
                 ))
     (add-hook 'dired-mode-hook
               (lambda ()
                 ;; Set dired-x buffer-local variables here.  For example:
                 ;; (dired-omit-mode 1)
                 ))


;;Company
;;AUCTEX
(company-auctex-init)
;;BIBTEX
(add-to-list 'company-backends 'company-bibtex)
;;C-HEADERS
(add-to-list 'company-backends 'company-c-headers)
;;EMOJI
(add-to-list 'company-backends 'company-emoji)
;;FLOW
(add-to-list 'company-backends 'company-flow)
;;PHP
(add-hook 'php-mode-hook
          '(lambda ()
            (require 'company-php)
            (company-mode t)
            (add-to-list 'company-backends 'company-ac-php-backend )))


;;Highlight Parentheses
(add-hook 'highlight-parentheses-mode-hook
          '(lambda ()
             (setq autopair-handle-action-fns
                   (append
                    (if autopair-handle-action-fns
              autopair-handle-action-fns
            '(autopair-default-handle-action))
          '((lambda (action pair pos-before)
              (hl-paren-color-update)))))))

(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

;;Enable Helm
(setq helm-mode t)

;;Enable Rainbow Mode
(setq rainbow-mode t)

;;enable Wordcount
(setq wc-mode t)

;;Enable writegood
(setq writegood-mode t)

;;disable spell-fu
;;(setq! global-spell-fu-mode 'nil)
;;disabled in package

;;Ispell change wordlist
(setq ispell-minor-mode t)
(defun fd-switch-dictionary()
  (interactive)
  (let* ((dic ispell-current-dictionary)
         (change (if (string= dic "deutsch8") "english" "deutsch8")))
    (ispell-change-dictionary change)
    (message "Dictionary switched from %s to %s" dic change)))



;;Disable line highlighting
(remove-hook! (prog-mode text-mode conf-mode special-mode) #'hl-line-mode)

;;Auto new line
(add-hook 'org-mode-hook '(lambda () (setq fill-column 80)))
(add-hook 'org-mode-hook 'auto-fill-mode)

;;Keycast
(use-package! keycast
  :commands keycast-mode
  :config
  (define-minor-mode keycast-mode
    "Show current command and its key binding in the mode line."
    :global t
    (if keycast-mode
        (progn
          (add-hook 'pre-command-hook 'keycast-mode-line-update t)
          (add-to-list 'global-mode-string '("" mode-line-keycast " ")))
      (remove-hook 'pre-command-hook 'keycast-mode-line-update)
      (setq global-mode-string (remove '("" mode-line-keycast " ") global-mode-string))))
  (custom-set-faces!
    '(keycast-command :inherit doom-modeline-debug
                      :height 0.9)
    '(keycast-key :inherit custom-modified
                  :height 1.1
                  :weight bold)))
(setq keycast-mode 't)


;;Discord
(setq elcord-mode 't)


;;Dired Icons
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)

(setq memento-mori-birth-date "2002-11-10")
(setq mementotmoritmode 't)
